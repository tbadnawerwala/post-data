import {combineReducers} from 'redux';
import postReducer from './postReducer';

const allReducers = combineReducers({
    postData: postReducer
})

export default allReducers;