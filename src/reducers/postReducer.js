const postReducer = (state={data:[]}, action)=>{
  
    switch(action.type){
        case 'SHOW_ASYNC':
            return{
                ...state,data: action.payload
            }
            
        default:
            return state;
    }
}

export default postReducer;