import { takeEvery, put, delay, call } from "redux-saga/effects";

function* changeAsync() {
    //yield delay(400);
    
    try{
        const response = yield call(fetch, 'https://jsonplaceholder.typicode.com/posts');
        
        const responseBody = yield response.json();
        
        yield put({ type: "SHOW_ASYNC" , payload: responseBody});
    }
    catch{
        //yield put(fetchFailed(e));
        return;
    }
    
  }
  
  export function* changeSaga() {
    yield takeEvery("SHOW", changeAsync);
  }