import './App.css';
import Nav from './components/nav';
import Post from './components/post';
import { useSelector, useDispatch } from "react-redux";
import { showPosts } from '../src/actions';
import { useEffect } from 'react';

function App() {

 

  return (
    <div className="App">
      <Nav />
      <Post />
    </div>
  );
}

export default App;
