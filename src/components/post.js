import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { useSelector, useDispatch } from "react-redux";
import { showPosts } from "../actions";
import Loader from "react-loader-spinner";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    maxWidth: 275,
    height: 280,
    display: "inline-block",
    margin: "2%",
  },

  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  titleData: {
    fontSize: "1rem",
  },
  pos: {
    marginBottom: 12,
  },
});

const Post = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(showPosts());
    setTimeout(() => {
      setLoading(true);
    }, 500);
  }, []);

  // dispatch(showPosts())
  const [isLoading, setLoading] = useState(false);
  const data = useSelector((state) => state.postData);
  console.log("data", data.data);

  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  if (isLoading) {
    console.log("jheya");
    return (
   
      data.data.map((item) => {
        return (
          <Card className={classes.root}>
            <CardContent>
              <Typography
                className={classes.title}
                color="textSecondary"
                gutterBottom
              >
                Title:
              </Typography>
              <Typography
                className={classes.titleData}
                variant="h5"
                component="h2"
              >
                {item.title}
                <br />
              </Typography>
              <br />
              {/* <Typography className={classes.pos} color="textSecondary">
              adjective
            </Typography> */}
              <Typography>
                Description: <br />
              </Typography>
              <Typography variant="body2" component="p">
                {item.body}
                <br />
              </Typography>
            </CardContent>
            <CardActions>
              {/* <Button size="small">Learn More</Button> */}
            </CardActions>
          </Card>
        );
      })
    );
  } else {
    return (
      <div style={{ textAlign: "center" }}>
        <Loader type="Puff" color="#00BFFF" height={100} width={100} />
      </div>
    );
  }
};

export default Post;
